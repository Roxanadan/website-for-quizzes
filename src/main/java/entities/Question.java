package entities;

import java.util.HashSet;
import java.util.Set;

public class Question {
    private Long Id;
    private String question;
    private String content;
    private boolean correct;
    private Set<Answer> answers = new HashSet<Answer>(0);

    public Question(){}

    public Question(String content){
        this.content = content;
    }
    public Question(String content, Set<Answer> answers){
        this.content = content;
        this.answers = answers;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }
}


