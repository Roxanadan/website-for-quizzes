package entities;

public enum RoleType {
    ADMIN, USER, STUDENT, TEACHER
}
