package services;

import entities.Role;
import entities.RoleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.RoleRepository;

@Service
public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    public Role save(Role role) {
        Role roleInDatabase = roleRepository.findRoleByName(role.getName());
        if (roleInDatabase != null) {
            throw new RuntimeException("Role already exists");
        } else {
             return roleRepository.save(role);
        }
    }

    public Role findByName(RoleType roleType) {
        return roleRepository.findRoleByName(roleType);
    }

}
