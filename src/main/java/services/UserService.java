package services;

import entities.Role;
import entities.RoleType;
import entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import repositories.RoleRepository;
import repositories.UserRepository;

import java.util.List;

public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;


    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User save(User user) {
        String initialPassword = user.getPassword();
        String encodedPassword = bCryptPasswordEncoder.encode(initialPassword);
        user.setPassword(encodedPassword);
        if (user.getRole() == null){
            Role userRole = roleRepository.findRoleByName(RoleType.USER);
            user.setRole(userRole);
        }
        return userRepository.save(user);
    }

    public User findByUsername(String username) {
        User userFromDatabase = userRepository.findByUsername(username);
        return userFromDatabase;
    }

}
